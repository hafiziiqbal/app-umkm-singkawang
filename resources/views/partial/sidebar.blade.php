<!-- Sidebar -->
<div class="w3-sidebar w3-light-grey w3-bar-block" style="width:25%">
    <div class="container pt-5" style="margin-top:20%">
        <a href="{{ route('usaha.create') }}"
            class="w3-bar-item btn text-end p-4 {{ $tittle == 'CREATE' ? 'btn-light' : '' }}">Pendaftaran
            UKM</a>
        <a href="{{ route('usaha.index') }}"
            class="w3-bar-item btn text-end p-4 {{ $tittle == 'LIST UMKM' ? 'btn-light' : '' }}">Data List UKM</a>
        <a href="{{ route('sms.index') }}"
            class="w3-bar-item btn text-end p-4 {{ $tittle == 'PESAN MASAL' ? 'btn-light' : '' }}">Kirim Pesan
            Masal</a>
        <form method="POST" action="{{ route('logout') }}">
            @csrf

            <x-responsive-nav-link class="w3-bar-item btn text-end p-4 text-danger" :href="route('logout')" onclick="event.preventDefault();
                                    this.closest('form').submit();">
                {{ __('Log Out') }}
            </x-responsive-nav-link>
        </form>

    </div>

</div>
