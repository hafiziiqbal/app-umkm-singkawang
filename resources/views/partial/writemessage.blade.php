<div id="card-sms">
    <div class="floating-div">
        <div class="center card-pesan">
            <a href="{{ route('sms.index') }}"><button style="float: right" type="button" class="btn-close"
                    aria-label="Close"></button></a>
            <h3 class=" fs-4 mb-3"><b>Pesan yang ingin dikirim</b></h3>

            <form action="{{ route('sms.store') }}" method="post">
                @csrf
                @foreach (session('userId') as $userId)
                    <input type="hidden" name="user_id[]" value="{{ $userId }}">
                @endforeach
                <textarea style="height: 150px" class="form-control mb-3" placeholder="Leave a comment here"
                    id="floatingTextarea2" style="height: 100px" name="message"></textarea>
                <button type="submit" class="btn btn-dark">Submit Pesan</button>
            </form>
        </div>
    </div>
</div>
