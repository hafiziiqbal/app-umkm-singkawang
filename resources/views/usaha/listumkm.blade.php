@extends('layouts.main')

@section('container')

    <div style="margin-right:30%">
        @if (session('message'))
            <div style="width: 100%" class="alert alert-success">{{ session('message') }}</div>
        @endif

        @if (session('messageError'))
            <div style="width: 100%" class="alert alert-danger">{{ session('messageError') }}</div>
        @endif

        <h2 class="fw-bold mb-5">
            Data List UKM Singkawang
        </h2>


        <table class="table">
            <thead>
                <tr>
                    <th scope="col">NO</th>
                    <th scope="col">Nama Pemilik Usaha</th>
                    <th scope="col">Nama Usaha</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($daftarUsaha as $key => $usaha)
                    <tr>
                        <th scope="row">{{ $key + 1 }}</th>
                        <td>{{ $usaha->user->nama }}</td>
                        <td>{{ $usaha->nama_usaha }}</td>
                        <td>
                            <form onsubmit="return confirm('Apakah Anda Yakin ?');"
                                action="{{ route('usaha.destroy', $usaha->id) }}" method="POST">
                                <a href="{{ route('usaha.edit', $usaha->id) }}"
                                    class="btn btn-sm btn-outline-warning">EDIT</a>
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-sm btn-outline-danger">HAPUS</button>
                            </form>
                        </td>
                    </tr>

                @endforeach
            </tbody>
        </table>




    </div>








@endsection
