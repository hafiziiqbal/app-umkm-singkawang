@extends('layouts.main')

@section('container')
    <div style="margin-right:30%">
        @if (session('messageError'))
            <div style="width: 100%" class="alert alert-danger">{{ session('messageError') }}</div>
        @endif

        @if (session('sendFail'))
            @foreach (session('sendFail') as $error)
                <div style="width: 100%" class="alert alert-danger">{{ $error }}</div>
            @endforeach

        @endif

        @if (session('message'))
            <div style="width: 100%" class="alert alert-success">{{ session('message') }}</div>
        @endif

        <h2 class="fw-bold mb-5">
            Data List UKM Singkawang
        </h2>

        <form action="{{ route('sms.write') }}" method="post">
            @csrf
            <button style="float: right; width:10rem" type="submit" class="btn btn-dark  mb-3 p-2">Kirim
                Pesan</button>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">NO</th>
                        <th scope="col">Nama Pemilik Usaha</th>
                        <th scope="col">Nama Usaha</th>
                        <th scope="col">Pilih</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($daftarUsaha as $key => $usaha)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $usaha->user->nama }}</td>
                            <td>{{ $usaha->nama_usaha }}</td>
                            <td>
                                <input class="form-check-input userId" type="checkbox" id="user-Id" name="userid[]"
                                    value={{ $usaha->user_id }}>
                            </td>
                        </tr>

                    @endforeach
                </tbody>
        </form>


        </table>


    </div>

@endsection
