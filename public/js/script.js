function deleteCard() {
    var value = parseInt(document.getElementById('numberizin').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('numberizin').value = value;

    var mycard = document.getElementById('izin' + value);
    mycard.remove();
}

function deleteCardLogo() {
    var mycard = document.getElementById('card-logo');

    const cardbox = document.querySelector('.cardlogo');
    const newinputlogo = document.createElement('input');
    newinputlogo.className = 'form-control';
    newinputlogo.type = 'file';
    newinputlogo.id = 'formFile';
    newinputlogo.name = 'logo';
    newinputlogo.required = true;
    cardbox.appendChild(newinputlogo);
    mycard.remove();
}

function deleteCardProduk(cardrm, pathproduk) {
    var value = parseInt(document.getElementById('numberproduk').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('numberproduk').value = value;

    var path = document.getElementById(pathproduk).value;
    const cardpath = document.querySelector('.pathproduk');
    const newInputPath = document.createElement('input');
    newInputPath.type = 'hidden';
    newInputPath.name = 'imgproduk_id[' + value + ']';
    newInputPath.value = path;
    cardpath.appendChild(newInputPath);

    var mycard = document.getElementById(cardrm);
    mycard.remove();
}

function deleteCardLokasi(cardrm, pathlokasi) {
    var value = parseInt(document.getElementById('numberlokasi').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('numberlokasi').value = value;

    var path = document.getElementById(pathlokasi).value;
    const cardpath = document.querySelector('.pathlokasi');
    const newInputPath = document.createElement('input');
    newInputPath.type = 'hidden';
    newInputPath.name = 'imglokasi_id[' + value + ']';
    newInputPath.value = path;
    cardpath.appendChild(newInputPath);

    var mycard = document.getElementById(cardrm);
    mycard.remove();
}

function addIzinCard() {

    var value = parseInt(document.getElementById('number').value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById('number').value = value;

    const cardbox = document.querySelector('.izin-card');
    const newcardBox1 = document.createElement('div');
    newcardBox1.className = 'card p-2 mb-4';

    const newbuttonclose = document.createElement('button');
    newbuttonclose.className = 'close-izin btn-close';
    newbuttonclose.type = 'button';
    newbuttonclose.label = 'Close';
    newbuttonclose.onclick = function () {
        this.parentElement.remove();
    };

    const newinputnumber = document.createElement('input');
    newinputnumber.className = 'form-control p-2 mb-3';
    newinputnumber.type = 'number';
    newinputnumber.placeholder = 'KBLI';
    newinputnumber.name = 'perizinan[' + (value) + '][kbli_id]';
    newinputnumber.required = true;

    const newcard = document.createElement('div');
    newcard.className = 'd-flex mb-3';

    const newinputdate = document.createElement('input');
    newinputdate.type = 'date';
    newinputdate.className = 'form-control p-2 me-3';
    newinputdate.name = 'perizinan[' + (value) + '][tanggal]';
    newinputdate.required = true;

    const newinputnumber1 = document.createElement('input');
    newinputnumber1.name = 'perizinan[' + (value) + '][iumk]';
    newinputnumber1.type = 'number';
    newinputnumber1.className = 'form-control p-2';
    newinputnumber1.placeholder = 'IUMK No.';
    newinputnumber1.required = true;

    cardbox.appendChild(newcardBox1);
    newcardBox1.appendChild(newbuttonclose);
    newcardBox1.appendChild(newinputnumber);
    newcardBox1.appendChild(newcard);
    newcard.appendChild(newinputdate);
    newcard.appendChild(newinputnumber1);
}

function addImg(cardName, nameImg, numbername) {
    /////////////////////////////
    var value = parseInt(document.getElementById(numbername).value, 10);
    value = isNaN(value) ? 0 : value;
    value++;
    document.getElementById(numbername).value = value;

    //var count = 1;
    const cardImg = document.querySelector(cardName);

    const newcardbox = document.createElement('div');
    newcardbox.className = 'card me-3';

    const newButtonClose = document.createElement('button');
    newButtonClose.className = 'close-izin btn-close';
    newButtonClose.type = 'button';
    newButtonClose.label = 'Close';
    newButtonClose.onclick = function () {
        this.parentElement.remove();
    };

    const newCardBody = document.createElement('div');
    newCardBody.className = 'card-body';

    const newInputFile = document.createElement('input');
    newInputFile.className = 'form-control form-control-sm';
    newInputFile.type = 'file';
    newInputFile.accept = 'image/*';
    newInputFile.name = nameImg + '[' + value + ']';
    newInputFile.required = true;

    cardImg.appendChild(newcardbox);
    newcardbox.appendChild(newButtonClose);

    newcardbox.appendChild(newCardBody);
    newCardBody.appendChild(newInputFile);
    /////////////////////////////
}

var loadFile = function (event) {
    var output = document.getElementById('output');
    output.src = URL.createObjectURL(event.target.files[0]);
    output.onload = function () {
        URL.revokeObjectURL(output.src) // free memory
    }
};
