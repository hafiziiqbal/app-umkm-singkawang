<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerizinansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('perizinan', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal');
            $table->string('iumk_nomor');

            //Foreign Key dari tabel usaha
            $table->foreignId('usaha_id')->constrained('usaha')->cascadeOnDelete();

            //foreign key dari tabel kbli            
            $table->foreignId('kbli_id')->constrained('kbli');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('perizinan');
    }
}
