<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsahasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usaha', function (Blueprint $table) {
            $table->id();

            //forign id dari tabel user
            $table->foreignId('user_id')->constrained('users')->cascadeOnDelete();;

            $table->string('nama_usaha');
            $table->bigInteger('nib');
            $table->string('jenis_usaha');

            //foreign key dari tabel jenis badan usaha
            $table->foreignId('jenis_badan_usaha_id')->constrained('jenis_badan_usaha');

            $table->string('alamat_usaha');
            $table->string('aset');
            $table->integer('rata_omset_perbulan');
            $table->integer('karyawan_lk');
            $table->integer('karyawan_pr');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usaha');
    }
}
