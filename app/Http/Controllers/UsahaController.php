<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRequestUsaha;
use App\Http\Requests\UpdateRequestUsaha;
use App\Models\Foto;
use App\Models\JenisBadanUsaha;
use App\Models\Perizinan;
use App\Models\Usaha;
use App\Models\User;

class UsahaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftarUsaha = Usaha::with('user')->get();
        return view('usaha.listumkm')->with(['tittle' => 'LIST UMKM', 'daftarUsaha' => $daftarUsaha]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $daftarJenisBadanUsaha = JenisBadanUsaha::all();
        return view('usaha.create', ['tittle' => 'CREATE', 'daftarJenisBadanUsaha' => $daftarJenisBadanUsaha]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequestUsaha $request)
    {

        //input user
        $user = new User;
        $user->nik            = $request->nik;
        $user->nama           = $request->nama;
        $user->jenis_kelamin  = $request->jenis_kelamin;
        $user->no_hp          = $request->no_hp;
        $user->alamat         = $request->alamat;
        $user->password       = bcrypt(strtolower(preg_replace("/[^a-zA-Z]/", "", $request->nama)) . "123");
        $user->save();

        //menetapkan role pengusaha
        $userById = User::find($user->id);
        $userById->assignRole('pengusaha');

        //input data usaha
        $usaha = new Usaha(
            [
                'nama_usaha'            => $request->nama_usaha,
                'nib'                   => $request->nib,
                'jenis_usaha'           => $request->jenis_usaha,
                'jenis_badan_usaha_id'  => $request->jenis_badan_usaha,
                'alamat_usaha'          => $request->alamat_usaha,
                'aset'                  => $request->aset,
                'rata_omset_perbulan'   => $request->rata_omset_perbulan,
                'karyawan_lk'           => $request->karyawan_lk,
                'karyawan_pr'           => $request->karyawan_pr,
            ]
        );
        $saveUsaha =  $userById->usaha()->save($usaha);

        //input data izin
        foreach ($request->perizinan as $izin) {
            $perizinan = new Perizinan(
                [
                    'tanggal'       => $izin['tanggal'],
                    'iumk_nomor'    => $izin['iumk'],
                    'kbli_id'       => $izin['kbli_id']
                ]
            );
            $usahaById = Usaha::find($saveUsaha->id);
            $usahaById->perizinan()->save($perizinan);
        }

        // input gambar logo
        $imgLogoName = 'logo_' . $request->file('logo')->hashName();
        $request->file('logo')->storeAs('public/uploads/img', $imgLogoName);
        $gambarLogo = new Foto(
            [
                'foto'          => 'storage/uploads/img/' . $imgLogoName,
                'jenis_foto'    => "logo",
                'nama_foto'     => "Logo " . $saveUsaha->nama_usaha
            ]
        );
        $usahaById = Usaha::find($saveUsaha->id);
        $usahaById->gambar()->save($gambarLogo);

        //input gambar produk
        foreach ($request->file('produk') as $key => $imgProduk) {
            $imgProdukName      = 'produk_' . $imgProduk->hashName();
            $imgProduk->storeAs('public/uploads/img', $imgProdukName);
            $gambarProduk  = new Foto(
                [
                    'foto'          => 'storage/uploads/img/' . $imgProdukName,
                    'jenis_foto'    => "produk",
                    'nama_foto'     => "Produk " . $key + 1
                ]
            );
            $usahaById = Usaha::find($saveUsaha->id);
            $usahaById->gambar()->save($gambarProduk);
        }

        //input gambar lokasi
        foreach ($request->file('lokasi') as $key => $imgLokasi) {
            $imgLokasiName      = 'lokasi_' . $imgLokasi->hashName();
            $imgLokasi->storeAs('public/uploads/img', $imgLokasiName);
            $gambarLokasi  = new Foto(
                [
                    'foto'          => 'storage/uploads/img/' . $imgLokasiName,
                    'jenis_foto'    => "lokasi",
                    'nama_foto'     => "Lokasi " . $key + 1
                ]
            );
            $usahaById = Usaha::find($saveUsaha->id);
            $usahaById->gambar()->save($gambarLokasi);
        }

        return redirect()->route('usaha.create')->with(['message' => 'Usaha Baru Berhasil Diinput']);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Mendapatkan daftar jenis badan usaha
        $daftarJenisBadanUsaha = JenisBadanUsaha::all();

        //Mendapatkan relasi usaha   
        $relasiUsahaById = Usaha::where('id', $id)->with('user', 'jenisBadanUsaha', 'gambar', 'perizinan', 'perizinan.kbli')->get();

        //Jika gambar tidak ada maka tampilkan gambar alternatif
        if ($relasiUsahaById[0]->gambar == []) {
            $logo = 'img/picture.png';
            $lokasi[] = '';
            $produk[] = '';
        }

        foreach ($relasiUsahaById[0]->gambar as $gambar) {
            //Jika logo ada, masukan ke variabel logo
            if ($gambar->jenis_foto === 'logo') {
                $logo = $gambar->foto;
            }

            //Jika jenis foto tidak ada logo, maka buat logo alternatif
            if ($gambar->jenis_foto != 'logo') {
                $logoAlt = 'img/picture.png';
            }

            //Jika gambar produk ada maka masukan ke array produk, jika tidak maka beri string kosong
            if ($gambar->jenis_foto === 'produk') {
                $produk[] = $gambar;
            } else {
                $produk[] = '';
            }

            //Jika gambar lokasi ada maka masukan ke array produk, jika tidak maka beri string kosong
            if ($gambar->jenis_foto === 'lokasi') {
                $lokasi[] = $gambar;
            } else {
                $lokasi[] = '';
            }
        }

        return view('usaha.edit', ['tittle' => 'EDIT', 'daftarJenisBadanUsaha' => $daftarJenisBadanUsaha, 'relasiUsahaById' => $relasiUsahaById, "logo" => (isset($logo) ? $logo : $logoAlt), "lokasi" => $lokasi, "produk" => $produk]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequestUsaha $request, $id)
    {
        //Mencari Usaha Sesuai Id Usaha
        $usahaById = Usaha::find($id);
        $userById = User::find($usahaById->user_id);

        // Update User
        $password = bcrypt(strtolower(preg_replace("/[^a-zA-Z]/", "", $request->nama)) . "123");
        $arrayUser = array(

            'nik'           => $request->nik,
            'nama'          => $request->nama,
            'jenis_kelamin' => $request->jenis_kelamin,
            'no_hp'         => $request->no_hp,
            'alamat'        => $request->alamat,
            'password'      => $password

        );
        $userById->update($arrayUser);

        //Update data usaha
        $arrayUsaha = array(
            'nama_usaha'            => $request->nama_usaha,
            'nib'                   => $request->nib,
            'jenis_usaha'           => $request->jenis_usaha,
            'jenis_badan_usaha_id'  => $request->jenis_badan_usaha,
            'alamat_usaha'          => $request->alamat_usaha,
            'aset'                  => $request->aset,
            'rata_omset_perbulan'   => $request->rata_omset_perbulan,
            'karyawan_lk'           => $request->karyawan_lk,
            'karyawan_pr'           => $request->karyawan_pr,
        );
        $userById->usaha()->update($arrayUsaha);

        //update perizinan
        if ($request->has('perizinan')) {
            //Hapus Perizinan
            $perizinanByUsahaId = Perizinan::where('usaha_id', $id)->get();
            foreach ($perizinanByUsahaId as $perizinanByUsahaId) {
                $perizinanByUsahaId->delete();
            }
            //Update data izin
            foreach ($request->perizinan as $izin) {
                $izin = new Perizinan(
                    [
                        'tanggal'       => $izin['tanggal'],
                        'iumk_nomor'    => $izin['iumk'],
                        'kbli_id'       => $izin['kbli_id']
                    ]
                );
                $usahaById->perizinan()->save($izin);
            }
        }

        //Update LOGO
        if ($request->has('logo')) {
            //Hapus Gambar
            $gambarByUsahaId = Foto::where(['usaha_id' => $id, 'jenis_foto' => 'logo'])->get();

            if ($gambarByUsahaId->all() != null) {
                $imagePath = public_path() . '/' . $gambarByUsahaId[0]->foto;
                unlink($imagePath);
                $gambarByUsahaId[0]->delete();
            }


            // Update gambar logo
            $imgLogoName = 'logo_' . $request->file('logo')->hashName();
            $request->file('logo')->storeAs('public/uploads/img', $imgLogoName);
            $gambarLogo = new Foto(
                [
                    'foto'          => 'storage/uploads/img/' . $imgLogoName,
                    'jenis_foto'    => "logo",
                    'nama_foto'     => "Logo " . $usahaById->nama_usaha
                ]
            );
            $usahaById->gambar()->save($gambarLogo);
        }

        //hapus gambar produk
        if ($request->has('imgproduk_id')) {
            foreach ($request->imgproduk_id as $key => $produkId) {
                $gambarById = Foto::where(['id' => $produkId, 'jenis_foto' => 'produk'])->get();
                $imagePath = public_path() . '/' . $gambarById[0]->foto;
                unlink($imagePath);
                $gambarById[0]->delete();
            }
        }

        //Update gambar produk
        if ($request->has('produk')) {

            foreach ($request->file('produk') as $key => $imgProduk) {
                $imgProdukName      = 'produk_' . $imgProduk->hashName();
                $imgProduk->storeAs('public/uploads/img', $imgProdukName);
                $gambarProduk  = new Foto(
                    [
                        'foto'          => 'storage/uploads/img/' . $imgProdukName,
                        'jenis_foto'    => "produk",
                        'id_data_usaha' => $usahaById->id,
                        'nama_foto'     => "Produk " . $key + 1
                    ]
                );
                $usahaById->gambar()->save($gambarProduk);
            }
        }

        //hapus gambar lokasi
        if ($request->has('imglokasi_id')) {
            foreach ($request->imglokasi_id as $key => $lokasiId) {
                $gambarById = Foto::where(['id' => $lokasiId, 'jenis_foto' => 'lokasi'])->get();
                $imagePath = public_path() . '/' . $gambarById[0]->foto;
                unlink($imagePath);
                $gambarById[0]->delete();
            }
        }

        if ($request->has('lokasi')) {
            //Update gambar lokasi
            foreach ($request->file('lokasi') as $key => $imgLokasi) {
                $imgLokasiName      = 'lokasi_' . $imgLokasi->hashName();
                $imgLokasi->storeAs('public/uploads/img', $imgLokasiName);
                $gambarLokasi  = new Foto(
                    [
                        'foto'          => 'storage/uploads/img/' . $imgLokasiName,
                        'jenis_foto'    => "lokasi",
                        'id_data_usaha' => $usahaById->id,
                        'nama_foto'     => "Lokasi " . $key + 1
                    ]
                );
                $usahaById->gambar()->save($gambarLokasi);
            }
        }

        return redirect()->route('usaha.index')->with(['message' => 'Usaha berhasil diupdate']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $usaha = Usaha::find($id);
        $user = User::find($usaha->user_id);

        $fotoByUsahaId = Foto::where('usaha_id', $id)->get();
        foreach ($fotoByUsahaId as $foto) {
            $imagePath = public_path() . '/' . $foto->foto;
            unlink($imagePath);
        }

        $hapusSuccess = $user->delete();

        if (!$hapusSuccess) {
            return redirect()->route('usaha.index')->with(['messageError' => 'Usaha ' . $usaha->nama_usaha . ' gagal dihapus']);
        } else {
            return redirect()->route('usaha.index')->with(['message' => 'Usaha ' . $usaha->nama_usaha . ' berhasil dihapus']);
        }
    }
}
