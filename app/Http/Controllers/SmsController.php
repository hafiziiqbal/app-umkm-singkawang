<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\User;
use App\Models\Usaha;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use GuzzleHttp\Exception\ClientException;


class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $daftarUsaha = Usaha::with('user')->get();
        return view('sms.sms')->with(['tittle' => 'PESAN MASAL', 'daftarUsaha' => $daftarUsaha]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $messageError = [];
        foreach ($request->user_id as  $penerimaId) {
            $daftarUsers = User::where('id', $penerimaId)->get();
            $usaha = Usaha::where('user_id', $penerimaId)->get();

            foreach ($daftarUsers as  $user) {
                if (!preg_match('/[^+0-9]/', trim($user->no_hp))) {
                    // cek apakah no hp karakter 1-3 adalah +62
                    if (substr(trim($user->no_hp), 0, 3) == '+62') {
                        $hp = trim($user->no_hp);
                    }
                    // cek apakah no hp karakter 1 adalah 0
                    elseif (substr(trim($user->no_hp), 0, 1) == '0') {
                        $hp = '+62' . substr(trim($user->no_hp), 1);
                    }
                }
            }


            $to = $hp;
            $from = getenv("TWILIO_FROM");
            $message = $request->message;

            try {
                $client = new Client(['auth' => [getenv("TWILIO_SID"), getenv("TWILIO_TOKEN")]]);
                $client->post(
                    'https://api.twilio.com/2010-04-01/Accounts/' . getenv("TWILIO_SID") . '/Messages.json',
                    ['form_params' => [
                        'Body' => $message, //set message body
                        'To' => $to,
                        'From' => $from //we get this number from twilio
                    ]]
                );
            } catch (ClientException $e) {
                $messageError[] = 'Pesan ke ' . $usaha[0]->nama_usaha . ' gagal dikirim ';
                // $responseContents = $e->getResponse()->getBody()->getContents();
                // $responseToJson =  json_decode($responseContents);
                // return $responseToJson;
            }
        }

        if ($messageError == null) {
            return redirect()->route('sms.index')->with(['message' => 'Pesan Berhasil Dikirim']);
        } else {
            return redirect()->route('sms.index')->with(['sendFail' => $messageError]);
        }
    }

    public function writeSms(Request $request)
    {
        if (isset($request->userid)) {
            // $daftarUsaha = Usaha::with('user')->get();
            // return view('sms.sms')->with(['tittle' => 'PESAN MASAL', 'daftarUsaha' => $daftarUsaha, 'userId' => $request->userid]);
            return redirect()->route('sms.index')->with(['userId' => $request->userid]);
        } else {
            return redirect()->route('sms.index')->with(['messageError' => 'Pilih minimal satu usaha']);
        }
    }
}
