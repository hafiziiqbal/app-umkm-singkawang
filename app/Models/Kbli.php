<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kbli extends Model
{
    use HasFactory;
    protected $table = 'kbli';

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
