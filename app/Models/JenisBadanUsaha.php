<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisBadanUsaha extends Model
{
    use HasFactory;
    protected $table = 'jenis_badan_usaha';
    protected $fillable = [
        'nama'
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];
}
