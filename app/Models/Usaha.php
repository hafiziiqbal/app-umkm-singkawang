<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usaha extends Model
{
    use HasFactory;

    protected $table = 'usaha';

    protected $fillable = [
        'user_id',
        'nama_usaha',
        'nib',
        'jenis_usaha',
        'perizinan_id',
        'jenis_badan_usaha_id',
        'alamat_usaha',
        'aset',
        'rata_omset_perbulan',
        'karyawan_lk',
        'karyawan_pr',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function perizinan()
    {
        return $this->hasMany(Perizinan::class, 'usaha_id');
    }

    public function jenisBadanUsaha()
    {
        return $this->belongsTo(JenisBadanUsaha::class, 'jenis_badan_usaha_id');
    }

    public function gambar()
    {
        return $this->hasMany(Foto::class, 'usaha_id');
    }
}
