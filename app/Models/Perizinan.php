<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Perizinan extends Model
{
    use HasFactory;
    protected $table = 'perizinan';
    protected $fillable = [
        'tanggal',
        'iumk_nomor',
        'kbli_id',
        'usaha_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at'
    ];

    public function kbli()
    {
        return $this->belongsTo(Kbli::class, 'kbli_id');
    }
}
