<?php

use App\Http\Controllers\SmsController;
use App\Http\Controllers\UsahaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

Route::group(['middleware' => 'auth'], function () {
    Route::resource('usaha', UsahaController::class);
    Route::resource('sms', SmsController::class);
    Route::post('/sms/write/', [SmsController::class, 'writeSms'])->name('sms.write');;
});

require __DIR__ . '/auth.php';
